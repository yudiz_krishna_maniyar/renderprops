import { useState } from "react";

// only contain logic of component 
const Warraper = (props) => {
    
    const [store, setstore] = useState([]);

    const [mcq, setMcq] = useState([]);

    const [Score, setScore] = useState(0);

     const [disable, setDisable] = useState(false);

    const Handleclear = (id) => {
      const final = store.filter((i) => i.id !== id);
      setstore(final);
    };

    
    const handleclick = (id, option) => {
    
      if (store?.some((i) => i.id === id)) {
    
        setstore(
          store?.map((item) => (item.id === id ? { ...item, option } : item))
        );
         
      } else {
      
        setstore((store) => [...store, { id, option }]);
        
      }
      
    };
       

    const selected = (ans) => {
      return store?.some((i) => {
        return i.option === ans;
      });
    };

    const handleSubmit = () => {
      setDisable(true);
    store?.map((item) =>
      mcq?.map((i) => {
        if (i.id == item.id && i.correct == item.option) {
         setScore((Score) => Score + 1);
          
        } else{
          setstore()
        }
        })
      );
    };
 
    // render props 
    return props.Quize(
      mcq,
      setMcq,
      handleclick,
      handleSubmit,
      Score,
      Handleclear,
      store,
     disable,
     selected
    );

};
export default Warraper;
