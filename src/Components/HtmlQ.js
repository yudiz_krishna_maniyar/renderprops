import React, { useEffect} from "react";
import axios from "axios";
import { Button } from "react-bootstrap";



// props
const HtmlQ = ({
  mcq,
  setMcq,
  handleclick,
  handleSubmit,
  Score,
  Handleclear,
   store,
  disable,
 selected
  }) => {
  useEffect(() => {
    axios
      .get("http://localhost:4000/data")
      .then((response) => {
        setMcq(response?.data?.html);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);



  return (
    <>
      <div className="container">
        {mcq?.map((mcq, index) => (
          <div key={index}>
            <li>
              {mcq?.id} {mcq?.question}
              <div>
                {mcq?.options?.map((item, index) => (
                  <button
                    key={index}
                    disabled={disable}
                    className={
                      selected(item.option) ? "unselected" : "Selected"
                    }
                    onClick={() => handleclick(mcq?.id, item?.option)}
                  >
                    {item.option}
                  </button>
                ))}
              </div>
            </li>
            <Button
              disabled={disable}
              className="clear"
              onClick={() => Handleclear(mcq.id)}
            >
              clear
            </Button>
          </div>
        ))}

        <div style={{ margin: "1%" }}>
          <Button disabled={disable} onClick={() => handleSubmit()}>
            Submit{" "}
          </Button>
          <div className="score">{Score}/5</div>
        </div>
      </div>
    </>
  );
};

export default HtmlQ;
