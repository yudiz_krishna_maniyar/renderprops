import {Routes, Route} from 'react-router-dom';
import HtmlQ from '../Components/HtmlQ';
import JsQ from '../Components/JsQ';
import Warraper from '../Components/Warraper';
import ReactQ from '../Components/ReactQ';
import Home from '../Home/Home';

// warraper component pass function as props and return component
const Routese = () => {
  return (
    <Routes>
       {/* use of outlet  */}
      <Route path="/" element={  <Home  /> } >
    
 <Route index element={<Warraper Quize={( mcq, setMcq,handleclick, handleSubmit,Score,Handleclear,store,disable, selected ) => {
  return (<HtmlQ mcq={mcq} setMcq={setMcq} handleclick={handleclick} handleSubmit={handleSubmit} Score={Score} Handleclear={Handleclear}
     store={store} disable={disable} selected={selected}/>);
            }} />
          }/>

 <Route path="js" element={ <Warraper  Quize={( mcq, setMcq,handleclick,handleSubmit, Score, Handleclear,store,disable,selected) => {
 return (  <JsQ mcq={mcq} setMcq={setMcq} handleclick={handleclick} handleSubmit={handleSubmit} Score={Score}
   Handleclear={Handleclear} store={store} disable={disable} selected={selected} />);
              }} />
          } />

 <Route path="/react" element={ <Warraper Quize={(mcq, setMcq,handleclick,handleSubmit, Score, Handleclear,store,disable,selected) => {
   return (  <ReactQ mcq={mcq} setMcq={setMcq} handleclick={handleclick} handleSubmit={handleSubmit} Score={Score}
   Handleclear={Handleclear} store={store} disable={disable} selected={selected} />);
              }} />
          } />
      </Route>
    </Routes>
  );
};

export default Routese;
