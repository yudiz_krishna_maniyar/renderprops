import React from 'react'
import { Outlet } from 'react-router-dom'

function home() {
  return (
    <>
      <div>
        {/* use od outlet */}
        <Outlet />
      </div>
     
    </>
  );
}

export default home