import React from "react";
import { Container, Nav, Navbar} from "react-bootstrap";
import { Link } from "react-router-dom";

export const Header = (disable, handleSubmit) => {
  return (
    <>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#">RenderProps</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
             
              <Link
                style={{ padding: "6%", margin: "4%" }}
                className="link"
                to="/"
              >
                HTML
              </Link>

              <Link
                style={{ padding: "6%", margin: "4%" }}
                className="link"
                to="/js"
              >
                JS
              </Link>

              <Link
                style={{ padding: "6%", margin: "4%" }}
                className="link"
                to="/react"
              >
                REACT
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
     
    </>
  );
};
